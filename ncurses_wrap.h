//
// Created by muromi on 16/11/07.
//

#ifndef MAP_SIM_NCURSES_WRAP_H
#define MAP_SIM_NCURSES_WRAP_H

#include <ncurses.h>
#include <iostream>
typedef std::string String_Type;
class MiracleNcurses{
public:

    ~MiracleNcurses(){endwin();}


    void init(){
        setlocale(LC_ALL,"");
        initscr();
        noecho();
        cbreak();
        keypad(stdscr,true);
        //start_color();
        //init_pair(1,COLOR_WHITE,COLOR_BLACK);
        //bkgd(COLOR_PAIR(1));
        enable_color = has_colors();
        if (enable_color) {
            start_color();
            init_pair(1, COLOR_WHITE, COLOR_GREEN);
            init_pair(2, COLOR_WHITE, COLOR_BLUE);
        }
    }

    //Setter & Getter
    void set_locate(int y,int x){
        if(using_area_x > x)
            using_area_x = x;
        if(using_area_y > y)
            using_area_y = y;
        move(y,x);
    }
    void add_str(String_Type str){addstr(str.c_str());}
    int32_t get_keybord(){return getch();}
    int32_t get_screen_width(){return getmaxx(stdscr);}
    int32_t get_screen_high(){return getmaxy(stdscr);}
    int32_t get_usingarea_width(){ return using_area_x;}
    int32_t get_usingarea_high();

    //Window control...
    void erase(){::erase();}
    void refresh(){::refresh();}

private:
    int32_t using_area_x{0};
    int32_t using_area_y{0};
    bool enable_color{false};

};

#endif //MAP_SIM_NCURSES_WRAP_H
