//
// Created by muromi on 16/11/07.
//

#include <iostream>
#include <sstream>
#include "mapdebugger.h"
#include "ncurses_wrap.h"

MapDebugger map;
MiracleNcurses view;

void create_draw();

int main(){
    map.set_size(16,16);
    map.set_goal(6,6);
    map.set_start(0,0);
    map.init();
    map.make_walkmap();

    view.init();
    create_draw();
    //timeout(-1);
    //view.get_keybord();

    return 0;
}

void create_draw(){
    bool select_enter{false};
    MiracleNcurses window;
    int32_t select{0};
    int32_t key{0};
    int32_t x,y;
    x=y=0;

    while(1) {
        window.erase();
        window.set_locate(1,90);
        window.add_str("        MapDev - V 1.0 ");
        window.set_locate(3,90);
        window.add_str("--------Description is Control key-------");
        window.set_locate(4,90);
        window.add_str("YazirushiKey : Move *");
        window.set_locate(5,90);
        window.add_str("     w a s d : add/delete wall");
        window.set_locate(6,90);
        window.add_str("           r : Reload walkmap");
        window.set_locate(7,90);
        window.add_str("           c : Clear MAP");
        window.set_locate(9,90);
        window.add_str("           l : DEBUG : read REALWALL.TXT");
        window.set_locate(10,90);
        window.add_str("           ; : DEBUG : read MAPDATA.TXT");
        window.set_locate(12,90);
        window.add_str("           n : DEBUG : write REAL_MAP.TXT");
        window.set_locate(13,90);
        window.add_str("           m : DEBUG : write REALWALL.TXT");
        window.set_locate(14,90);
        window.add_str("           , : DEBUG : write MAPDATA.TXT");
        window.set_locate(16,90);
        window.add_str("          q : Quit..");
        window.set_locate(20,90);
        window.add_str("-------------Debug Status----------------");
        std::stringstream debug_1;
        debug_1 << "get_cnt_map():" << map.get_cnt_walkmap();
        window.set_locate(21,90);
        window.add_str(debug_1.str());
        std::stringstream debug_2;
        debug_2 << "get_searched():" << std::to_string(map.get_searched(x,y));
        window.set_locate(22,90);
        window.add_str(debug_2.str());

        map.draw_realmap(16, 16, 0, 0, x, y);
        key = window.get_keybord();
        if(select_enter)select_enter= false;
        switch (key){
            case 'y':select_enter=true;break;
            case KEY_UP:if(y<15)y++;break;
            case KEY_DOWN:if(y>0)y--;break;
            case KEY_LEFT:if(x>0)x--;break;
            case KEY_RIGHT:if(x<15)x++;break;
            case 'w':
                if(map.get_wall_realwall(x, y)&NORTH){
                    map.delete_wall_realwall(x, y, NORTH);
                    map.delete_wall_realwall(x, y + 1, SOUTH);
                }else{
                    map.set_wall_realwall(x, y, NORTH);
                    map.set_wall_realwall(x, y + 1, SOUTH);
                }
                map.make_walkmap();
                break;
            case 'a':
                if(map.get_wall_realwall(x, y)&WEST){
                    map.delete_wall_realwall(x, y, WEST);
                    map.delete_wall_realwall(x - 1, y, EAST);
                }else{
                    map.set_wall_realwall(x, y, WEST);
                    map.set_wall_realwall(x - 1, y, EAST);
                }
                map.make_walkmap();
                break;
            case 's':
                if(map.get_wall_realwall(x, y)&SOUTH){
                    map.delete_wall_realwall(x, y, SOUTH);
                    map.delete_wall_realwall(x, y - 1, NORTH);
                }else{
                    map.set_wall_realwall(x, y, SOUTH);
                    map.set_wall_realwall(x, y - 1, NORTH);
                }
                map.make_walkmap();
                break;
            case 'd':
                if(map.get_wall_realwall(x, y)&EAST){
                    map.delete_wall_realwall(x, y, EAST);
                    map.delete_wall_realwall(x + 1, y, WEST);
                }else{
                    map.set_wall_realwall(x, y, EAST);
                    map.set_wall_realwall(x + 1, y, WEST);
                }
                map.make_walkmap();
                break;


            case 'l':
                if(map.readfile_realwall() == -1)
                {
                    window.set_locate(20,90);
                    window.add_str(" LOG *** Sorry. ERROR in \"l : DEBUG : read REALWALL.TXT\"");
                }
                break;
            case ';':
                if(map.readfile_mapdata() == -1)
                {
                    window.set_locate(20,90);
                    window.add_str(" LOG *** Sorry. ERROR in \"; : DEBUG : read MAPDATA.TXT\"");
                }
                break;
            case 'n':
                if(map.writefile_realwall() == -1)
                {
                    window.set_locate(20,90);
                    window.add_str(" LOG *** Sorry. ERROR in \"n : DEBUG : write REALWALL.TXT\"");
                }
                break;
            case 'm':
                if(map.writefile_walkmap() == -1)
                {
                    window.set_locate(20,90);
                    window.add_str(" LOG *** Sorry. ERROR in \"m : DEBUG : write WALK_MAP.TXT\"");
                }
                break;
            case ',':
                if(map.writefile_mapdata() == -1)
                {
                    window.set_locate(20,90);
                    window.add_str(" LOG *** Sorry. ERROR in \", : DEBUG : write MAPDATA.TXT\"");
                }
                break;
            case 'r':
                map.up_debug(x,y);
                map.fillup_not_search();

                break;
            case 'c':
                map.init();
                map.make_walkmap();
                break;
            case 'q':return;

            default:
                select_enter = false;
                break;
        }
        window.refresh();
    }
}
