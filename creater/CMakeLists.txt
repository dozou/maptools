cmake_minimum_required(VERSION 3.6)
project(MapCreater)

set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -std=c++11")
include_directories(../)
set(SOURCE_FILES main.cpp ../mslm/map.cpp ../mslm/map.h ../mslm/deftype.h ../mapdebugger.h ../mapdebugger.cpp)
add_executable(MapCreater ${SOURCE_FILES})
target_link_libraries(MapCreater ncurses)