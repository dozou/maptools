#include <iostream>
#include "../mapdebugger.h"
#include "../ncurses_wrap.h"

MapDebugger map;



void view_title();
void view_map(int size_x, int size_y, int offset_x, int offset_y,int point_x,int point_y,MapDebugger &map);
void draw_submenu(int32_t select_num,bool enter);
void create_draw();

int main() {
    int x=0,y=0;
    int key;
    map.set_size(16,16);
    map.set_start(0,0);
    map.set_goal(7,7);
    map.init();
    MiracleNcurses window;
    window.init();
    view_title();
    create_draw();
    return 0;
}

void view_title(){
    MiracleNcurses window;
    String_Type title = "マップくりえーた〜ぁ ver 0.0.1";
    window.erase();
    window.set_locate(window.get_screen_high()/2,(window.get_screen_width()/2)-(title.size()/2) );
    window.add_str(title);
    window.refresh();
    timeout(-1);
    window.get_keybord();
}

void draw_submenu(int32_t select_num,bool enter){
    MiracleNcurses window;
    window.set_locate(4,100);
    window.add_str("------SubMenu(↑↓  R/F)----");
    //window.set_locate(5,100);

    window.set_locate(5,100);
    if(select_num==0)attrset(COLOR_PAIR(1)|A_REVERSE);
    window.add_str("Write mapdata.");
    attrset(COLOR_PAIR(1));

    window.set_locate(6,100);
    if(select_num==1)attrset(COLOR_PAIR(1)|A_REVERSE);
    window.add_str("Read mapdata.");
    attrset(COLOR_PAIR(1));

    window.set_locate(7,100);
    if(select_num==2){
        attrset(COLOR_PAIR(1)|A_REVERSE);
        if(enter){
            attrset(COLOR_PAIR(1));
            exit(0);
        }
    }
    window.add_str("Quit....");
    attrset(COLOR_PAIR(1));
}

void create_draw(){
    bool select_enter{false};
    MiracleNcurses window;
    int32_t select{0};
    int32_t key{0};
    int32_t x,y;
    x=y=0;

    while(1) {
        window.erase();
        draw_submenu(select,select_enter);
        view_map(16, 16, 0, 0, x, y,map);
        key = window.get_keybord();
        if(select_enter)select_enter= false;
        switch (key){
            case 'y':select_enter=true;break;
            case KEY_UP:if(y<15)y++;break;
            case KEY_DOWN:if(y>0)y--;break;
            case KEY_LEFT:if(x>0)x--;break;
            case KEY_RIGHT:if(x<15)x++;break;
            case 'w':
                if(map.get_wall_realwall(x,y)&NORTH){
                    map.delete_wall_realwall(x,y,NORTH);
                    map.delete_wall_realwall(x,y+1,SOUTH);
                }else{
                    map.set_wall_realwall(x,y,NORTH);
                    map.set_wall_realwall(x,y+1,SOUTH);
                }
                break;
            case 'a':
                if(map.get_wall_realwall(x,y)&WEST){
                    map.delete_wall_realwall(x,y,WEST);
                    map.delete_wall_realwall(x-1,y,EAST);
                }else{
                    map.set_wall_realwall(x,y,WEST);
                    map.set_wall_realwall(x-1,y,EAST);
                }
                break;
            case 's':
                if(map.get_wall_realwall(x,y)&SOUTH){
                    map.delete_wall_realwall(x,y,SOUTH);
                    map.delete_wall_realwall(x,y-1,NORTH);
                }else{
                    map.set_wall_realwall(x,y,SOUTH);
                    map.set_wall_realwall(x,y-1,NORTH);
                }
                break;
            case 'd':
                if(map.get_wall_realwall(x,y)&EAST){
                    map.delete_wall_realwall(x,y,EAST);
                    map.delete_wall_realwall(x+1,y,WEST);
                }else{
                    map.set_wall_realwall(x,y,EAST);
                    map.set_wall_realwall(x+1,y,WEST);
                }
                break;

            case 'r':
                if(select>0)select--;
                break;
            case 'f':
                if(select<2)select++;
                break;
            case 'q':return;

            default:
                select_enter = false;
                break;
        }
        window.refresh();
    }
}

void view_map(int size_x, int size_y, int offset_x, int offset_y,int point_x,int pointt_y,MapDebugger &map){
    MiracleNcurses window;
    String_Type str;
    String_Type west_wall = "| ";
    String_Type obj="  ";


    //Draw XY scale!!
    for (int y = size_y - 1; y >= 0; y--) {
        window.set_locate(y * 2 + offset_y + 1, offset_x);
        window.add_str(std::to_string(size_y - 1 - y));
    }
    for (int x = size_x - 1; x >= 0; x--) {
        window.set_locate(offset_y + size_y * 2 + 1, x * 5 + offset_x + 4);
        window.add_str(std::to_string(x));
    }

    for (int y = 0; y < size_y; y++)
        for (int x = 0; x < size_x; x++) {
            window.set_locate(y * 2 + offset_y, x * 5 + offset_x + 2);
            if(map.get_wall_realwall(x,size_y-1-y)&NORTH)window.add_str("+ --");
            else window.add_str("+   ");
            window.set_locate((y * 2) + 1 + offset_y, x * 5 + offset_x + 2);
            if(point_x==x && pointt_y == size_y-1-y)obj="■";
            else if(map.get_x_goal()==x && map.get_y_goal()==size_y-1-y)obj="GG";
            else obj="  ";
            if(map.get_wall_realwall(x,size_y-1-y)&WEST)west_wall="| ";
            else west_wall="  ";

            window.add_str(west_wall + obj);

        }

    for (int y = 0; y < size_y; y++) {
        window.set_locate((y * 2) + offset_y, size_y * 5 + offset_x + 2);
        window.add_str("+");
        window.set_locate((y * 2) + 1 + offset_y, size_y * 5 + offset_x + 2);
        window.add_str("|");
    }
    for (int x = 0; x < size_x; x++) {
        window.set_locate((size_y * 2) + offset_y, x * 5 + offset_x + 2);
        window.add_str("+ -- +");
    }
}
