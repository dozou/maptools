//
// Created by muromi on 16/11/07.
//

#ifndef MAP_SIM_MAP_PC_IO_H
#define MAP_SIM_MAP_PC_IO_H
#include "mslm/map.h"

class MapDebugger: public Map2{
public:

    void draw_realmap(int size_x,int size_y,int offset_x,int offset_y,int point_x,int point_y);
    int readfile_realwall();
    //void read_virtualwall();
    int readfile_walkmap();
    int readfile_mapdata();
    int writefile_realwall();
    //int writefile_virtualwall();
    int writefile_walkmap();
    int writefile_mapdata();

private:
};


#endif //MAP_SIM_MAP_PC_IO_H
