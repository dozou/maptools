//
// Created by muromi on 16/11/07.
//

#define FILE_NAME_REAL_WALL ("REALWALL.TXT")
#define FILE_NAME_VIRTUAL_WALL ("V_WALL.TXT")
#define FILE_NAME_WALK_MAP ("WALK_MAP.TXT")
#define FILE_NAME_MAPDATA ("MAPDATA.TXT")

#include "mapdebugger.h"
#include "ncurses_wrap.h"
#include <sstream>
#include <iomanip>

void MapDebugger::draw_realmap(int size_x, int size_y, int offset_x, int offset_y,int point_x,int point_y){
    MiracleNcurses window;
    //String_Type str;
    //String_Type west_wall = "|";
    //String_Type obj="   ";

    attron(COLOR_PAIR(2));
    //Draw XY scale!!
    for (int y = size_y - 1; y >= 0; y--) {
        window.set_locate(y * 2 + offset_y + 1, offset_x);
        window.add_str(std::to_string(size_y - 1 - y));
    }
    for (int x = size_x - 1; x >= 0; x--) {
        window.set_locate(offset_y + size_y * 2 + 1, x * 5 + offset_x + 4);
        window.add_str(std::to_string(x));
    }
    attroff(COLOR_PAIR(2));

    for (int y = 0; y < size_y; y++)
        for (int x = 0; x < size_x; x++) {
            window.set_locate(y * 2 + offset_y, x * 5 + offset_x + 2);
            window.add_str("+ ");
            if(get_wall_realwall(x,size_y-1-y)&NORTH) {
                attron(A_BOLD); window.add_str("--"); attroff(A_BOLD);
            } else {
                window.add_str(" ");
            }

            window.set_locate((y * 2) + 1 + offset_y, x * 5 + offset_x + 2);
            if(get_wall_realwall(x,size_y-1-y)&WEST) {
                attron(A_BOLD); window.add_str("| "); attroff(A_BOLD);
            } else {
                window.add_str("  ");
            }

            if(get_x_goal()==x && get_y_goal()==size_y-1-y) {
                attron(COLOR_PAIR(1) | A_BOLD); window.add_str("GG"); attroff(COLOR_PAIR(1) | A_BOLD);
            } else {
                auto v = get_cnt_walkmap(x,size_y-1-y);

                std::stringstream _obj;
                _obj << std::setfill(' ') << std::setw(3) << std::to_string(v);

                if (point_x==x && point_y==size_y-1-y) attron(A_REVERSE | A_BOLD);
                if (v != 255)
                    window.add_str(_obj.str());
                else
                    window.add_str("   ");

                attroff(A_REVERSE | A_BOLD);
            }
        }

    for (int y = 0; y < size_y; y++) {
        window.set_locate((y * 2) + offset_y, size_y * 5 + offset_x + 2);
        window.add_str("+");
        window.set_locate((y * 2) + 1 + offset_y, size_y * 5 + offset_x + 2);

        if (get_wall_realwall(15,size_y-1-y)&EAST) {
            attron(A_BOLD); window.add_str("| "); attroff(A_BOLD);
        } else
            window.add_str("  ");
    }

    for (int x = 0; x < size_x; x++) {
        window.set_locate((size_y * 2) + offset_y, x * 5 + offset_x + 2);

        window.add_str("+ ");
        if (get_wall_realwall(x, 0)&SOUTH) {
            attron(A_BOLD); window.add_str("--"); attroff(A_BOLD);
        } else {
            window.add_str("  ");
        }
        window.add_str(" +");
    }
}



int MapDebugger::readfile_realwall()
{
    int i,j;
    FILE *fp;
    if((fp = fopen(FILE_NAME_REAL_WALL,"r")) == NULL) return -1;

    uint8_t buf;
    for(j = 0; j <=15; j++){
        for(i = 0; i <= 15; i++){
            fscanf(fp, "%hd ", &buf);
            set_wall_realwall(i, j, buf);
        }
    }
fclose(fp);
return 0;
}
int MapDebugger::readfile_walkmap()
{
    int i,j;
    FILE *fp;
    if((fp = fopen(FILE_NAME_WALK_MAP,"r")) == NULL) return -1;

    uint8_t buf;
    for(j = 0; j <=15; j++){
        for(i = 0; i <= 15; i++){
            fscanf(fp, "%hd ", &buf);
            set_cnt_walkmap(i, j, buf);
        }
    }
    fclose(fp);
    return 0;
}
int MapDebugger::readfile_mapdata()
{
    int i,j;
    FILE *fp;
    if((fp = fopen(FILE_NAME_MAPDATA,"r")) == NULL) return -1;

    uint8_t buf8;
    int16_t buf16;
    for(j = 0; j <=15; j++){
        for(i = 0; i <= 15; i++){
            fscanf(fp, "%hd", &map->walk_map[i+1][j+1]);
        }
    }
    for(j = 0; j <=15; j++){
        for(i = 0; i <= 15; i++){
            fscanf(fp, "%hd ", &buf8);
            set_wall_realwall(i, j, buf8);
        }
    }
    fclose(fp);

    return 0;
}
// --- WRITE --- //
int MapDebugger::writefile_realwall() {
    int i,j;
    FILE *fp;
    if((fp = fopen(FILE_NAME_REAL_WALL,"w")) == NULL) return -1;

    for(j = 0; j <=15; j++){
        for(i = 0; i <= 15; i++){
            fprintf(fp, "%d ", get_wall_realwall(i, j));
        }
    }
    fclose(fp);
    return 0;
}
int MapDebugger::writefile_walkmap() {
    int i,j;
    FILE *fp;
    if((fp = fopen(FILE_NAME_WALK_MAP,"w")) == NULL) return -1;

    for(j = 0; j <=15; j++){
        for(i = 0; i <= 15; i++){
            fprintf(fp, "%d ", get_cnt_walkmap(i, j));
        }
    }
    fclose(fp);
    return 0;
}
int MapDebugger::writefile_mapdata() {
    int i,j;
    FILE *fp;
    if((fp = fopen(FILE_NAME_MAPDATA,"w")) == NULL) return -1;

    for(j = 0; j <=15; j++){
        for(i = 0; i <= 15; i++){
            fprintf(fp, "%d ", get_cnt_walkmap(i, j));
        }
    }
    fprintf(fp,"\r\n");
    for(j = 0; j <=15; j++){
        for(i = 0; i <= 15; i++){
            fprintf(fp, "%d ", get_wall_realwall(i, j));
        }
    }
    fclose(fp);
    return 0;
}